#ifndef IMAGE_ROTATION_IO_H
#define IMAGE_ROTATION_IO_H

#include <stdbool.h>
#include <stdio.h>

bool file_open(FILE** file, char *name, char *mode);
bool file_close(FILE* file);

#endif //IMAGE_ROTATION_IO_H
