#ifndef IMAGE_ROTATION_BMP_HEADER_H
#define IMAGE_ROTATION_BMP_HEADER_H

#include <inttypes.h>
#include <malloc.h>
#include <stdint.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


struct pixel { uint8_t b, g, r; };

struct image {
    size_t width;
    size_t height;
    struct pixel *data;
};

struct bmp_header* header_create(void);

struct image* image_create(size_t width, size_t height);

void header_free( struct bmp_header *header);

void image_free( struct image *image );

uint8_t padding_calc(struct image *image);

struct bmp_header* get_header_from_img(struct image *image);

#endif //IMAGE_ROTATION_BMP_HEADER_H

