#ifndef IMAGE_ROTATION_BMP_IMAGE_H
#define IMAGE_ROTATION_BMP_IMAGE_H
#include "bmp_header.h"
#include <stdbool.h>
#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* *img);

enum write_status to_bmp(FILE* out, struct image *img);

struct pixel get_pix(struct image *image, size_t idx);

void write_pix(struct image* *rotated_image, struct pixel px, size_t idx);

void rotate(struct image *image, struct image* *new_image);

#endif //IMAGE_ROTATION_BMP_IMAGE_H
