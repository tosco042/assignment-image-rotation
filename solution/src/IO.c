#include "IO.h"

bool file_open(FILE** file, char *name, char *mode) {
    (*file) = fopen(name, mode);
    return ( (*file) != NULL);
}

bool file_close(FILE* file) {
    return fclose(file) == 0;
}
