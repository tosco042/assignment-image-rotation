#include "IO.h"
#include "bmp_header.h"
#include "bmp_image.h"

int main(int argc, char **argv) {
    if (argc<3) {
        fprintf(stderr, "Invalid args.");
        return -1;
    }
    FILE *file_read = NULL;
    FILE *file_write = NULL;
    file_open(&file_read, argv[1], "rb");
    file_open(&file_write, argv[2], "wb");
    struct image *image = NULL;
    enum read_status readStatus = from_bmp(file_read, &image);
    if (readStatus!=READ_OK) {
        fprintf(stderr, "Read error");
        return readStatus;
    }
    struct image *new_image = NULL;
    rotate(image, &new_image);
    image_free(image);
    enum write_status writeStatus = to_bmp(file_write, new_image);
    if (writeStatus!=WRITE_OK) {
        fprintf(stderr, "Write error");
        image_free(new_image);
        return writeStatus;
    }
    image_free(new_image);
    if (!file_close(file_read) || !file_close(file_write)) {
        fprintf(stderr, "Close files error");
        return -1;
    }
    return 0;
}
