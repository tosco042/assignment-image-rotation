#include "bmp_image.h"
#include "bmp_header.h"

bool padding_skip(FILE *in, struct image *image) {
    if (fseek(in, padding_calc(image), SEEK_CUR)) {
        return false;
    }
    return true;
}

bool header_read(FILE *f, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

bool image_read(FILE *in, struct image *image) {
    for (size_t height = 0; height<image->height; height++) {
        for (size_t width = 0; width<image->width; width++) {
            if (!fread((image->data+(height*(image->width)+width)), sizeof(struct pixel), 1, in)) {
                return false;
            }
        }
        if (!padding_skip(in, image)) {
            return false;
        }
    }
    return true;
}

bool write_header(FILE *out, struct bmp_header *header) {
    return fwrite(header, sizeof(struct bmp_header), 1, out);
}

bool write_padding(FILE *out, struct image *image) {
    if (padding_calc(image) == 0) return true;
    uint8_t padding[3] = {0, 0, 0};
    return fwrite(&padding, sizeof(uint8_t), padding_calc(image), out);
}

bool write_image(FILE *out, struct image *image) {
    for (size_t h = 0; h < image->height; h++) {
        for (size_t w = 0; w < image->width; w++) {
            if (!fwrite((image->data + h * (image->width) + w), sizeof(struct pixel), 1, out)) {
                return false;
            }
        }
        if (!write_padding(out, image)) {
            return false;
        }
    }
    return true;
}

enum read_status from_bmp(FILE *in, struct image* *img) {
    struct bmp_header *header = header_create();
    if (!header_read(in, header)) {
        header_free(header);
        return READ_INVALID_HEADER;
    }
    *img = image_create(header->biWidth, header->biHeight);
    if (!image_read(in, *img)) {
        header_free(header);
        return READ_INVALID_BITS;
    }
    header_free(header);
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    struct bmp_header *header = get_header_from_img(img);
    if (!write_header(out, header)) {
        header_free(header);
        return WRITE_ERROR;
    }
    if (!write_image(out, img)) {
        header_free(header);
        return WRITE_ERROR;
    }
    header_free(header);
    return WRITE_OK;
}

struct pixel get_pix(struct image *image, size_t idx) {
    return image->data[idx];
}

void write_pix(struct image* *rotated_image, struct pixel px, size_t idx) {
    (*rotated_image)->data[idx]=px;
}

void rotate(struct image *image, struct image* *new_image) {
    *new_image = image_create(image->height, image->width);
    for (size_t width = 0; width<image->width; width++) {
        for (size_t height = 0; height<image->height; height++) {
            write_pix(new_image, get_pix(image,width+image->width*(image->height-1-height)), height+width*(image->height));
        }
    }
}

