#include "bmp_header.h"

size_t get_img_size(struct image *image){
    return image->width+padding_calc(image)*image->height;
}

size_t get_file_size(struct image *image) {
    return sizeof(struct bmp_header) +
           sizeof(struct pixel)*image->width*image->height +
           padding_calc(image)*image->height;
}

struct bmp_header* header_create(void) {
    return malloc(sizeof(struct bmp_header));
}

struct image* image_create(size_t width, size_t height) {
    struct pixel *pix = malloc(sizeof(struct pixel)*width*height);
    struct image *im = malloc(sizeof(struct image));
    im->height = height;
    im->width = width;
    im->data = pix;
    return im;
}

void header_free( struct bmp_header *header) {
    free(header);
}

void image_free( struct image *image ) {
    free(image->data);
    image->data = NULL;
    free(image);
}

uint8_t padding_calc(struct image *image) {
    if (image->width % 4 != 0) {
        return (uint8_t)(4-((image->width*sizeof(struct pixel))%4));
    }
    return 0;
}

struct bmp_header* get_header_from_img(struct image *image) {
    struct bmp_header *header = header_create();
    header->bfType = 0x4D42;
    header->bfileSize = get_file_size(image);
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = 40;
    header->biWidth = image->width;
    header->biHeight = image->height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = get_img_size(image);
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
    return header;
}
